/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * All rights reserved.
 * Portions Copyright 2019 Michael Grunditz
 * Portions Copyright 2019 John Ballance
 * Use is subject to license terms.
 */

/*
 * Simple printf for writing to HAL debug port
 * const char* fmt, the formatting string followed by the data to be formatted
 *
 *
 * Formatting strings <fmt>
 * %B    - binary (d = 0b1000001)
 * %b    - binary (d = 1000001)
 * %c    - character (s = H)
 * %d/%i - integer (d = 65)\
 * %f    - float (f = 123.45)
 * %3f   - float (f = 123.346) three decimal places specified by %3.
 * %s    - char* string (s = Hello)
 * %X    - hexadecimal (d = 0x41)
 * %x    - hexadecimal (d = 41)
 * %%    - escaped percent ("%")
 */
//#include "global/HALEntries.h"
//#include "swis.h"
#include "stdarg.h"
#include "stdio.h"
#include "haldebugp.h"
__global_reg(6) void *sb;

extern void HAL_DebugTX(char c);
void halput(char c);
void halputstr(char *c);

void halput(char c)
{
  HAL_DebugTX(c);
}

void halputstr(char *c)
{
  while(*c)halput(*(c++));
}

void halprintf(const char* fmt, ...)
{
    va_list argv;
    va_start(argv, fmt);
    char buf[256]; // make sure this is big enough for any sprintf commands below

    for (int i = 0; fmt[i] != '\0'; i++)
    {
      if (fmt[i] == '%')
      {
            // Look for specification of number of decimal digits
            int places = 0;
            if (fmt[i+1] >= '0' && fmt[i+1] <= '9') {
                places = fmt[i+1] - '0';
                i++;
            }
             switch (fmt[++i]) {
                  case 'O':
                      halputstr("0o"); // Fall through intended
                    case 'o':
                      sprintf(buf,"%o",va_arg(argv, int));
                      halputstr(buf);
                      break;
                  case 'c':
                      halput((char) va_arg(argv, int));
                      break;
                  case 'd':
                  case 'i':
                      sprintf(buf,"%d",va_arg(argv, int));
                      halputstr(buf);
                      break;
                  case 'p':
                      sprintf(buf,"%8X",va_arg(argv, int));
                      halputstr(buf);
                      break;
                  case 's':
                      halputstr((char*)va_arg(argv, const char*));
                      break;
                  case 'X':
                      halputstr("0x"); // Fall through intended
                  case 'x':
                      sprintf(buf,"%*x",places,va_arg(argv, int));
                      halputstr(buf);
                      break;
                  case '%':
                      halput(fmt[i]);
                      break;
                  default:
                      halput('?');
                      break;
              }
      }
      else
      {
        halput(fmt[i]);
      }
    }
    va_end(argv);
}
