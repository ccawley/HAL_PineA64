/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * cddl/RiscOS/Sources/HWSupport/SD/SDIODriver/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2012 Ben Avison.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef REGISTER_H
#define REGISTER_H

/* Register bit layout for selected SDHCI registers which have been reused
 * as HAL device API bitfields */

/* capabilities[0] bits: commented-out bits are not currently read by SDIODriver */
//#define CAP0_ST_SHIFT   (30)
//#define CAP0_ST_REMOV   (0u << CAP0_ST_SHIFT)  /* Removable card slot */
//#define CAP0_ST_EMBED   (1u << CAP0_ST_SHIFT)  /* Embedded slot for one device */
//#define CAP0_ST_SHARED  (2u << CAP0_ST_SHIFT)  /* Shared bus slot */
//#define CAP0_ST_MASK    (3u << CAP0_ST_SHIFT)
//#define CAP0_AI         (1u<<29)  /* Asynchronous interrupt  */
//#define CAP0_64SB       (1u<<28)  /* 64-bit system bus support */
#define CAP0_VS18       (1u<<26)  /* Voltage support 1.8V */
#define CAP0_VS30       (1u<<25)  /* Voltage support 3.0V */
#define CAP0_VS33       (1u<<24)  /* Voltage support 3.3V */
//#define CAP0_SRS        (1u<<23)  /* Suspend/resume support */
//#define CAP0_DS         (1u<<22)  /* DMA support */
#define CAP0_HSS        (1u<<21)  /* High speed (52MHz) support */
//#define CAP0_ADMA1      (1u<<20)  /* ADMA1 support */
//#define CAP0_ADMA2      (1u<<19)  /* ADMA2 support */
//#define CAP0_8BIT       (1u<<18)  /* 8-bit embedded device  */
#define CAP0_MBL_SHIFT  (16)
#define CAP0_MBL_512    (0u << CAP0_MBL_SHIFT)  /* 512-byte blocks */
#define CAP0_MBL_1024   (1u << CAP0_MBL_SHIFT)  /* 1024-byte blocks */
#define CAP0_MBL_2048   (2u << CAP0_MBL_SHIFT)  /* 2048-byte blocks */
#define CAP0_MBL_MASK   (3u << CAP0_MBL_SHIFT)
//#define CAP0_BCF_SHIFT  (8)
//#define CAP0_BCF_MASK   (0xFFu << CAP0_BCF_SHIFT)
//#define CAP0_TCU        (1u<<7)  /* Timeout clock unit */
//#define CAP0_TCF_SHIFT  (0)
//#define CAP0_TCF_MASK   (0x3Fu << CAP0_TCF_SHIFT)

/* capabilities[1] bits: commented-out bits are not currently read by SDIODriver */
//#define CAP1_CM_SHIFT   (16)
//#define CAP1_CM_MASK    (0xFFu << CAP1_CM_SHIFT)
//#define CAP1_RTM_SHIFT  (14)
//#define CAP1_RTM_MODE1  (0u << CAP1_RTM_SHIFT)
//#define CAP1_RTM_MODE2  (1u << CAP1_RTM_SHIFT)
//#define CAP1_RTM_MODE3  (2u << CAP1_RTM_SHIFT)
//#define CAP1_RTM_MASK   (3u << CAP1_RTM_SHIFT)
//#define CAP1_UTSDR50    (1u<<13)
//#define CAP1_TCR_SHIFT  (8)
//#define CAP1_TCR_MASK   (0xF << CAP1_TCR_SHIFT)
//#define CAP1_DTDS       (1u<<6)
//#define CAP1_DTCS       (1u<<5)
//#define CAP1_DTAS       (1u<<4)
#define CAP1_DDR50S     (1u<<2)
#define CAP1_SDR104S    (1u<<1)
#define CAP1_SDR50S     (1u<<0)

#define HCV_SREV_SHIFT  (0)
#define HCV_SREV_1_00   (0x00u << HCV_SREV_SHIFT)  /* SD Host Specification Version 1.00 */
#define HCV_SREV_2_00   (0x01u << HCV_SREV_SHIFT)  /* SD Host Specification Version 2.00 */
#define HCV_SREV_3_00   (0x02u << HCV_SREV_SHIFT)  /* SD Host Specification Version 3.00 */
#define HCV_SREV_MASK   (0xFFu << HCV_SREV_SHIFT)  /* Specification version number */

/* transfer_mode bits: commented-out bits are currently not read by A64SD */
//#define TM_MSBS         (1u<<5)  /* Multi / single block select */
#define TM_DDIR         (1u<<4)  /* Data transfer direction select */
#define TM_ACEN_SHIFT   (2)
//#define TM_ACEN_NONE    (0u << TM_ACEN_SHIFT) /* Auto command disabled */
#define TM_ACEN_CMD12   (1u << TM_ACEN_SHIFT) /* Auto CMD12 enable */
//#define TM_ACEN_CMD23   (2u << TM_ACEN_SHIFT) /* Auto CMD23 enable */
#define TM_ACEN_MASK    (3u << TM_ACEN_SHIFT)
//#define TM_BCE          (1u<<1)  /* Block count enable */
//#define TM_DE           (1u<<0)  /* DMA enable */

/* command bits: commented-out bits are currently not read by A64SD */
#define CMD_INDEX_SHIFT  (8)
#define CMD_INDEX_MASK   (0x3Fu << CMD_INDEX_SHIFT)
#define CMD_TYPE_SHIFT   (6)
//#define CMD_TYPE_NORMAL  (0u << CMD_TYPE_SHIFT)
//#define CMD_TYPE_SUSPEND (1u << CMD_TYPE_SHIFT)
//#define CMD_TYPE_RESUME  (2u << CMD_TYPE_SHIFT)
#define CMD_TYPE_ABORT   (3u << CMD_TYPE_SHIFT)
#define CMD_TYPE_MASK    (3u << CMD_TYPE_SHIFT)
//#define CMD_DP           (1u<<5)  /* Data present */
#define CMD_CICE         (1u<<4)  /* Command index check enable */
#define CMD_CCCE         (1u<<3)  /* Command CRC check enable */
#define CMD_RSP_SHIFT    (0)
#define CMD_RSP_NONE     (0u << CMD_RSP_SHIFT) /* No response */
#define CMD_RSP_136      (1u << CMD_RSP_SHIFT) /* 136-bit response */
#define CMD_RSP_48       (2u << CMD_RSP_SHIFT) /* 48-bit response */
#define CMD_RSP_48_BUSY  (3u << CMD_RSP_SHIFT) /* 48-bit response, check for busy after response */
#define CMD_RSP_MASK     (3u << CMD_RSP_SHIFT)


/***********/
/* WARNING */
/***********/
/* The following do not form part of the HAL device API, therefore the fact that
 * the code relies on them indicates a probable bug */

/* interrupt bits */
#define IRQ_TC   (1u<<1)  /* Transfer complete */
#define IRQ_CC   (1u<<0)  /* Command complete */

#endif
