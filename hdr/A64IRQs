; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2019 Michael Grunditz
; Portions Copyright 2019 John Ballance
; Use is subject to license terms.
;
;
;
;

  [     :LNOT: :DEF: __HAL_A64IRQs_HDR__
        GBLL    __HAL_A64IRQs_HDR__
; Mapping of all defined IRQ numbers in the Allwinner A64 chip
; NOTE  vector 0 actually starts at offset 32 ....
IRQ_SGI0             *           (0   +32)
IRQ_SGI1             *           (1   +32)
IRQ_SGI2             *           (2   +32)
IRQ_SGI3             *           (3   +32)
IRQ_SGI4             *           (4   +32)
IRQ_SGI5             *           (5   +32)
IRQ_SGI6             *           (6   +32)
IRQ_SGI7             *           (7   +32)
IRQ_SGI8             *           (8   +32)
IRQ_SGI9             *           (9   +32)
IRQ_SGI10            *           (10  +32)
IRQ_SGI11            *           (11  +32)
IRQ_SGI12            *           (12  +32)
IRQ_SGI13            *           (13  +32)
IRQ_SGI14            *           (14  +32)
IRQ_SGI15            *           (15  +32)
IRQ_PPI0             *           (16  +32)
IRQ_PPI1             *           (17  +32)
IRQ_PPI2             *           (18  +32)
IRQ_PPI3             *           (19  +32)
IRQ_PPI4             *           (20  +32)
IRQ_PPI5             *           (21  +32)
IRQ_PPI6             *           (22  +32)
IRQ_PPI7             *           (23  +32)
IRQ_PPI8             *           (24  +32)
IRQ_PPI9             *           (25  +32)
IRQ_PPI10            *           (26  +32)
IRQ_PPI11            *           (27  +32)
IRQ_PPI12            *           (28  +32)
IRQ_PPI13            *           (29  +32)
IRQ_PPI14            *           (30  +32)
IRQ_PPI15            *           (31  +32)
;
IRQ_UART0            *           (32  +32)
IRQ_UART1            *           (33  +32)
IRQ_UART2            *           (34  +32)
IRQ_UART3            *           (35  +32)
IRQ_UART4            *           (36  +32)
;
IRQ_TWI0             *           (38  +32)
IRQ_TWI1             *           (39  +32)
IRQ_TWI2             *           (40  +32)
;
IRQ_PB_EINT          *           (43  +32)
IRQ_OWA              *           (44  +32)
IRQ_I2S_PCM0         *           (45  +32)
IRQ_I2S_PCM1         *           (46  +32)
IRQ_I2S_PCM2         *           (47  +32)
;
IRQ_Timer0           *           (50  +32)
IRQ_Timer1           *           (51  +32)
;
IRQ_PG_EINT          *           (53  +32)
;
IRQ_Watchdog         *           (57  +32)
;
IRQ_AC_DET           *           (60  +32)
IRQ_AudioCodec       *           (61  +32)
IRQ_KEYADC           *           (62  +32)
IRQ_ThermalSensor    *           (63  +32)
IRQ_EXT_NMI          *           (64  +32)
IRQ_R_Timer0         *           (65  +32)
IRQ_R_Timer1         *           (66  +32)
;
IRQ_R_watchdog       *           (68  +32)
IRQ_R_CIR_RX         *           (69  +32)
IRQ_R_UART           *           (70  +32)
IRQ_R_RSB            *           (71  +32)
IRQ_R_Alarm0         *           (72  +32)
IRQ_R_Alarm1         *           (73  +32)
IRQ_R_Timer2         *           (74  +32)
IRQ_R_Timer3         *           (75  +32)
IRQ_R_TWI            *           (76  +32)
IRQ_R_PL_EINT        *           (77  +32)
IRQ_R_TWD            *           (78  +32)
;
IRQ_MSGBOX           *           (81  +32)
IRQ_DMA              *           (82  +32)
IRQ_HSTimer          *           (83  +32)
;
IRQ_SMC              *           (88  +32)
;
IRQ_VE               *           (90  +32)
;
IRQ_SD_MMC0          *           (92  +32)
IRQ_SD_MMC1          *           (93  +32)
IRQ_SD_MMC2          *           (94  +32)
;
IRQ_SPI0             *           (97  +32)
IRQ_SPI1             *           (98  +32)
;
IRQ_DRAM_MDFS        *           (101 +32)
IRQ_NAND             *           (102 +32)
IRQ_USB_OTG          *           (103 +32)
IRQ_USB_OTG_EHCI     *           (104 +32)
IRQ_USB_OTG_OHCI     *           (105 +32)
IRQ_EHCI0            *           (106 +32)
IRQ_OHCI0            *           (107 +32)
;
IRQ_CE0              *           (112 +32)
IRQ_TE               *           (113 +32)
IRQ_EMAC             *           (114 +32)
IRQ_SCR              *           (115 +32)
IRQ_CSI              *           (116 +32)
IRQ_CSI_CCI          *           (117 +32)
IRQ_TCON0            *           (118 +32)
IRQ_TCON1            *           (119 +32)
IRQ_HDMI             *           (120 +32)
IRQ_MIPI_DSI         *           (121 +32)
;
IRQ_DIT              *           (125 +32)
IRQ_CE1              *           (126 +32)
IRQ_DE               *           (127 +32)
IRQ_ROT              *           (128 +32)
IRQ_GPU_GP           *           (129 +32)
IRQ_GPU_GPMMU        *           (130 +32)
IRQ_GPU_PP0          *           (131 +32)
IRQ_GPU_PP0MMU       *           (132 +32)
IRQ_GPU_PMU          *           (133 +32)
IRQ_GPU_PP1          *           (134 +32)
IRQ_GPU_PP1MMU       *           (135 +32)
;
IRQ_CTI0             *           (140 +32)
IRQ_CTI1             *           (141 +32)
IRQ_CTI2             *           (142 +32)
IRQ_CTI3             *           (143 +32)
IRQ_COMMTX0          *           (144 +32)
IRQ_COMMTX1          *           (145 +32)
IRQ_COMMTX2          *           (146 +32)
IRQ_COMMTX3          *           (147 +32)
IRQ_COMMRX0          *           (148 +32)
IRQ_COMMRX1          *           (149 +32)
IRQ_COMMRX2          *           (150 +32)
IRQ_COMMRX3          *           (151 +32)
IRQ_PMU0             *           (152 +32)
IRQ_PMU1             *           (153 +32)
IRQ_PMU2             *           (154 +32)
IRQ_PMU3             *           (155 +32)
IRQ_AXI_Error        *           (156 +32)




  ]

 END
