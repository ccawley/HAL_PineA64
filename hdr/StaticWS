; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2019 Michael Grunditz
; Portions Copyright 2019 John Ballance
; Use is subject to license terms.
;
;
;
;

        GET     DMA.hdr
        GET     Video.hdr
        GET     I2C.hdr
        GET     board.hdr
        GET     NIC.hdr
        GET     CPUClk.hdr
        GET     Hdr:GPIODevice
        GET     Hdr:SDHCIDevice
        GET     Hdr:BMUDevice
        GET     Hdr:RTCDevice
        GET     Hdr:DMADevice
        GET     Hdr:AudioDevice
        GET     Hdr:MixerDevice

sb              RN      9
v7              RN      10
v8              RN      11

; Use jack detect IRQ to auto-mute speakers when headphones are plugged in
                GBLL    JackDetect
JackDetect      SETL    {TRUE}

        MACRO
        CallOS  $entry, $tailcall
        ASSERT  $entry <= HighestOSEntry
 [ "$tailcall"=""
        MOV     lr, pc
 |
   [ "$tailcall"<>"tailcall"
        ! 0, "Unrecognised parameter to CallOS"
   ]
 ]
        LDR     pc, OSentries + 4*$entry
        MEND

; Per-SDHCI workspace .. needs to track the C definitions

                ^       0
SDHCIDevice     #       HALDevice_SDHCISize_Soft          ; see Hdr:SDHCIDevice
SDHCISlotInfo   #       HALDeviceSDHCI_SlotInfo_Size ; each of our controllers has just the 1 slot
SDHCISB         #       4              ; pointer to HAL workspace for HAL calls
SDHCIOpaque     #       4*16           ; private workspace for SMHCLib
SDCCUAddr       #       4              ; Logical address of CCU (clock control unit)
SDPCAddr        #       4              ; Logical address of CPUx port controller
SDPC2Addr       #       4              ; Logical address of CPUs port controller
SDClkReg        #       1              ; Address of SDMMCx_CLK_REG within CCU
SDSoftRstBit    #       1              ; Bit within BUS_SOFT_RST_REG0
SDPort          #       1              ; GPIO port configuring our pins
SDCmdPin        #       1              ; Pin within GPIO port corresponding to CMD signal
SDHCISize       *       :INDEX:@

MaxSDControllers *      3

; Per-BMU workspace

                ^       0
BMUDevice       #       HALDevice_BMU_Size           ; see Hdr:BMUDevice
BMUWS           #       4                            ; pointer to HAL workspace for HAL calls
BMUParams       #       4                            ; Per-device params
BMUSize         *       :INDEX:@

; Per-RTC workspace

                ^       0
RTCDevice       #       HALDevice_RTC_Size           ; see Hdr:RTCDevice
RTCDeviceHAL_SB #       4                            ; pointer to HAL workspace for HAL calls
RTCSize         *       :INDEX:@

; Per-DMA channel workspace
                       ^ 0, a1
DMACDevice             # HALDevice_DMAL_Size
DMACWorkspace          # 4 ; HAL workspace ptr
DMACChannelNumber      # 4 ; Physical channel number
DMACDRQ                # 4 ; DRQ this channel will be used with
DMACFlags              # 4 ; Flags from SetOptions call
DMACPeriphAddr         # 4 ; Peripheral address from SetOptions call
DMACDescriptorsPhys    # 4 ; Phys addr of descriptor array from SetListTransfer call
DMACDescriptorsLog     # 4 ; Log addr of descriptor array from SetListTransfer call
DMACDescriptorCount    # 4 ; Number of descriptors from SetListTransfer call
DMACLength             # 4 ; Transfer length from SetListTransfer call
DMACLoopLength         # 4 ; Byte length of each loop
DMACLastLoopCount      # 4 ; Loop count at last DMAC_ListTransferProgress call
DMACFinalProgress      # 4 ; Progress at time channel was disabled
DMACActive             # 4 ; Non-zero if we expect the channel to be active
DMACDesc               # 28 ; Buffer for description string
DMAC_DeviceSize        * :INDEX: @

                        ^ 0
MixerChannel_System     # 1 ; Main volume control
MixerChannel_Speaker    # 1 ; Internal speakers (line out)
MixerChannel_Headphones # 1 ; Headphone jack
Mixer_Channels          * :INDEX: @

                ^       0,sb
;BoardConfig     #       0 ;BoardConfig_Size ; NOTE: Almost all code assumes the
OSheader        #       4
OSentries       #       4*(HighestOSEntry+1)

SMHC0_Log       #       4
SMHC1_Log       #       4
SMHC2_Log       #       4
RTC_Log         #       4
MPU_INTC_Log    #       4 ; MPU_INTC logical address
Timers_Log      #       4 ; Timers logical base address

USB_Host_Log    #       4 ; USB_Host base address
USB_OTGHost_Log #       4 ; USB_OTGHost base address
USB_OTGDev_Log  #       4 ; USB_OTGDevice base address
GPMC_Regs_Log   #       4 ; GPMC_Regs base address
sys_clk         #       4 ; System clock speed in Hz
Timer_DelayMul  #       4 ; sys_clk/100KHz
HALInitialised  #       4 ; Flag for whether HAL_Init is done yet
NVMemoryFound   #       4 ; Size of EEPROM detected (may be 0)
L4_Display_Log  #       4 ; L4_Display base address

IntSRAM_Log     #       4 ; SRAM logical address
HDMI_Log        #       4

GRF_Log         #       4

PC_Log          #       4
PC2_Log         #       4
CCU_Log         #       4
RSB_Log         #       4
PMU_Log         #       4
PWM_Log         #       4
DE_Log          #       4
IRQ_Log         #       4
GPIO_Log        #       4
TIMER_Log       #       4
R_TWI_Log       #       4
TWI0_Log        #       4
TWI1_Log        #       4
TWI2_Log        #       4
PRCM_Log        #       4
DebugUART       #       4
DefRes          #       4
VIDCList3       #       0    ;VIDCList3_Size



; [ DebugInterrupts
LastInterrupt_IRQ #     4 ; Last IRQ, -1 if cleared
LastInterrupt_FIQ #     4 ; Last FIQ, -1 if cleared
 ; [ ExtraDebugInterrupts
;ExtraDebugIRQEnabled #  4 ; Nonzero if extra debugging enabled
;  ]
; ]
UART_Base       #       4*(UART_Count)       ; base hardware address of uarts
UART_IRQ        #       4*(UART_Count)       ; irq numbers for the uarts
UART_TIRQEn     #       1*(UART_Count)       ; record state of transmit irq enable
; align on 4 byte boundary
                #       (((:INDEX:@)+3):AND::NOT:3)-(:INDEX:@)

;UARTFCRSoftCopy #       4
NCNBWorkspace   #       4 ; Base of ncnb workspace
NCNBAllocNext   #       4 ; next free address in ncnb workspace
SDIOWS          #       SDHCISize * MaxSDControllers
RTCWS           #       RTCSize

VideoDevice     #       Video_DeviceSize
VideoBoardConfig #      VideoBoardConfig_Size


KbdMatrixScan   #       4 ; Scan results for boot-time keyboard matrix scanning
KbdMatrixTime   #       4 ; Start time of boot-time keyboard matrix scanning


NICWS           #       NIC_DeviceSize
CPUClkWS        #       CPUClk_WorkspaceSize
GPIOWS          #       HALDevice_GPIO_Size
NVRAMWS         #       HALDeviceSize
;RTCWS           #       RTCSize


BMUWS1          #       BMUSize
BMUWS2          #       BMUSize

                #       (((:INDEX:@)+15):AND::NOT:15)-(:INDEX:@) ; Align for easy ADR
AudioDevice     #       HALDevice_Audio_Size_1
                #       (((:INDEX:@)+15):AND::NOT:15)-(:INDEX:@)
MixerDevice     #       HALDevice_Mixer_Size_0_1
MixerSettings   #       8 * Mixer_Channels
AudioActive     #       1 ; Non-zero if audio device has been activated
AudioOn         #       1 ; Non-zero if audio is enabled
AudioRate       #       1 ; Sample rate index selected
 [ JackDetect
JackState       #       1 ; Zero if jack is in
 |
                #       1
 ]
AudioPLLRate    #       4 ; Current PLL rate

DMAFreeChannels #       4 ; Mask of which physical DMA channels are free
DMAChannelList  #       DMA_CH_count*4 ; List of channel devices for Enumerate
                #       (((:INDEX:@)+15):AND::NOT:15)-(:INDEX:@)
DMAController   #       HALDevice_DMAC_Size_0_1      ; see Hdr:HALDevice
DMAChannels     #       DMAC_DeviceSize*DMA_CH_count ; List of channel devices (indexed by physical channel #)


myHDMI_audioparams  #   0 ; start of hdmi_audioparam_s structure
IecCgmsA                  # 1
                          # 3
IecCopyright              # 4
IecCategoryCode           # 1
IecPcmMode                # 1
IecSourceNumber           # 1
IecClockAccuracy          # 1
OriginalSamplingFrequency # 4
ChannelAllocation         # 1
                          # 3
SamplingFrequency         # 4
SampleSize                # 4

;myidmacinfo         #   ipu_idmac_info_size ; used in ipu_idmac







;USBHAL_WS       #       USBHAL_WS_Size ; USB workspace for keyboard scan



                ;#       (((:INDEX:@)+3):AND::NOT:3)-(:INDEX:@)
                #       (((:INDEX:@)+15):AND::NOT:15)-(:INDEX:@)

;USBAllocAreaSize * 16*1024 ; With an ordinary setup, about half of this memory gets used. About 3K goes to some big allocs (looks like the bus structs)

;USBAllocArea    #       USBAllocAreaSize


;BoardConfig_Size *       :INDEX:@
HAL_WsSize      *       :INDEX:@

        END
