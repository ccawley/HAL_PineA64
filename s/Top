; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2019 Michael Grunditz
; Portions Copyright 2019 John Ballance
; Use is subject to license terms.
;
;
;


        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System
        GET     Hdr:Machine.<Machine>
        GET     Hdr:HALSize.<HALSize>

        GET     Hdr:MEMM.VMSAv6

        GET     Hdr:Proc
        GET     Hdr:OSEntries
        GET     Hdr:HALEntries
        ;GET     Hdr:VIDCList
        GET     PINEA64.hdr

        GET     StaticWS.hdr
        GET     UART.hdr
        GET     SDRC.hdr
        GET     GPIO.hdr

        AREA    |!!!ROMStart|, CODE, READONLY

        IMPORT  rom_checkedout_ok
        IMPORT  SoftCMOS
        EXPORT  HAL_Base
	;EXPORT  HAL_WsBase
        IMPORT  HAL_DebugTX
        IMPORT DebugHALPrint
        IMPORT DebugHALPrintReg
        MACRO
        CallOSM $entry, $reg
        LDR     ip, [v8, #$entry*4]
        MOV     lr, pc
        ADD     pc, v8, ip
        MEND

        ALIGN   4096



        ENTRY
HAL_Base
        B pineinit
        DCD     SoftCMOSStore-HAL_Base
end_stack
	EXPORT  HAL_WsBase
HAL_WsBase
        %       HAL_WsSize ;BoardConfig_Size

        ALIGN   512
SoftCMOSStore              ; space for sd based soft cmos
        %       256
        LTORG

        ;movw r1, #0x131   ;@ set NS, AW, FW, HVC

        ;mcr p15, 0, r1, c1, c1, 0  ;@ write SCR (with NS bit set)
pineinit
        DMB

	ldr v1, =pfetchhandlerjump

	 mcr p15, 0, v1, c12, c0, 0 ; vbar



	B	cpend
	LDR	v1,pfetchhandler
	LDR	v3,pfetchend

	SUBS	v2,v3,v1
	LDR	v3,=&0c
cphand1
	ADD	v1,v1,v2
	STR	v1,[v3,v2]
	SUBS	v2,v2,#1
	;TST	v2,#0
	BGT	cphand1
	LDR	v1,pfetchhandler
	LDR	v3,pfetchend
	SUBS	v2,v3,v1
	LDR	v3,=&14
cphand2
	ADD	v1,v1,v2
	STR	v1,[v3,v2]
	SUBS	v2,v2,#1
	;TST	v2,#0
	BGT	cphand2
cpend
                MRC p15, 0,v1, c1, c0, 0; Read SCTLR
	BIC	v1,v1,#&180000

	MCR p15, 0,v1, c1, c0, 0; Write SCTLR

        B foundconfig

        ;ENTRY



foundconfig

        MSR     CPSR_c, #F32_bit+I32_bit+SVC32_mode

        ; Now do common init
        B       restart

        ; Entry point for masquerading as a Linux kernel
        ; r0=0
        ; r1=machine type ID
        ; r2=ptr to tag list (linux/include/asm/setup.h)

; ------------------------------------------------------------------------------
; Perform some Cortex-A8 specific CPU setup
; Then start looking for RAM
; In: sb = board config ptr
restart


        ADRL    v1, HAL_Base + OSROM_HALSize    ; v1 -> RISC OS image

        LDR     v8, [v1, #OSHdr_Entries]
        ADD     v8, v8, v1                      ; v8 -> RISC OS entry table

        ; Ensure CPU is set up
        MOV     a1, #0
        CallOSM OS_InitARM

        ADRL    sb,HAL_WsBase
        ;ADRL    R13,end_stack
        ;ADRL    r0,HAL_Base
        ;MCR     p15,0,r0,c12,c0
        ;ADRL sb,BoardConfig
        LDR a1,=A64_UART_0
        STR a1,DebugUART
        MOV a2,#'X'
        STR a2,[a1]
        ;MOV a2,#'Y'
        ;STR a2,[sb,#:INDEX:DebugUART]
        ;DebugChar a3,a4,48
        MSR     CPSR_c, #F32_bit+I32_bit+SVC32_mode
        ;DebugChar a3,a4,49
        DebugTX "Hello"

       ;  MRC p15, 0,a1, c1, c0, 0; Read SCTLR
;	BIC	a1,a1,#4096

;	MCR p15, 0,a1, c1, c0, 0; Write SCTLR
        ;DebugChar a3,a4,50

docmosstuff
; The loader image on disc has a copy of the CMOS loaded at &200 before
; the base of the HAL. This is in CMOS Physical format, whereas the  HAL
; internally is mapped to logical addressing for ease of preloading
; so map as follows:
; Physical    Logical     note
; 00-1f       f0-ff       not used
; 10-3e       c0-ee       top part
; 3f          ef          cmos checksum
; 40-ff       00-bf       bottom part
; checksum = sum of all bytes from 10-ff other than byte at 3f
; this sum + 1 = value at 3f
        ; first validate the cmos in rom
        adrl    v1, SoftCMOSStore+&10
 DebugTX "start of cmos "
 DebugReg v1,"start of cmos "
        mov     v2, #&ef
        mov     v3, #1
cmosckloop
        ldrb    v4, [v1, v2]
        add     v3, v3, v4
        subs    v2, v2, #1
        bge     cmosckloop
        ldrb    v4, [v1, #&2f]          ; actual &3f from base
       DebugReg v4," v4 = "
       DebugReg v3," v3 = "
        sub     v3, v3, v4            ;, lsl #1
        and     v3, v3, #&ff
        teq     v3, v4                ; byte match?
        bne     nocmoscopy              ; checksum looks bad
        ; OK cmos is usable. copy into hal
       DebugReg v3," cksum worked "
        adrl    v3, SoftCMOS + &c0
        adrl    v1, SoftCMOSStore+&10
        mov     v2, #&2c
1       ldr     v4, [v1, v2]
        str     v4, [v3, v2]
        subs    v2, v2, #4
        bge     %bt1
        adrl    v3, SoftCMOS + &00
        adrl    v1, SoftCMOSStore+&40
        mov     v2, #&bc
2       ldr     v4, [v1, v2]
        str     v4, [v3, v2]
        subs    v2, v2, #4
        bge     %bt2
        b       %ft3
nocmoscopy
        DebugReg v3," cksum bad "

3	MOV     a1, #0
        MCR     p15, 0, a1, c7, c5, 0
        MCR     p15, 0, a1, c7, c5, 6
        DSB     SY ; Wait for I-cache invalidation to complete
        ISB     SY ; Wait for

;        DebugTX "Hello"


	B	rom_checkedout_ok


        LTORG
        GET     SMCSecure.s



pfetchhandlerjump
	ldr pc, pfetchhandler

	ldr pc, pfetchhandler

	ldr pc, pfetchhandler

	 ldr pc, pfetchhandler

	 ldr pc, pfetchhandler

	ldr pc, pfetchhandler

	ldr pc, pfetchhandler

pfetchhandler
	SUB 	lr,lr,#8
	DebugReg lr, "PC "
handloop
	B handloop
pfetchend


        END
