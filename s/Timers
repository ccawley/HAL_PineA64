; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2019 Michael Grunditz
; Portions Copyright 2019 John Ballance
; Portions Copyright 2022 Jeffrey Lee
; Use is subject to license terms.
;
;
;


        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System
        GET     Hdr:Machine.<Machine>
        GET     Hdr:ImageSize.<ImageSize>
        $GetIO
	GET     Hdr:Proc
        GET     Hdr:OSEntries
        GET     Hdr:HALEntries


        GET     PINEA64.hdr
        GET     StaticWS.hdr
        GET     Timers.hdr

        AREA    |Asm$$Code|, CODE, READONLY

        EXPORT  HAL_Timers
        EXPORT  HAL_TimerDevice
        EXPORT  HAL_TimerGranularity
        EXPORT  HAL_TimerMaxPeriod
        EXPORT  HAL_TimerSetPeriod
        EXPORT  HAL_TimerPeriod
        EXPORT  HAL_TimerReadCountdown
        EXPORT  HAL_TimerIRQClear

        EXPORT  HAL_CounterRate
        EXPORT  HAL_CounterPeriod
        EXPORT  HAL_CounterRead
        EXPORT  HAL_CounterDelay

        EXPORT  HAL_Watchdog

        EXPORT  Timer_Init

        IMPORT  __rt_udiv10

 [ Debug
        IMPORT DebugHALPrint
        IMPORT DebugHALPrintReg
 ]


Timer_Init
; Make sure all the timers are using the 24MHz source, and are actually enabled
        LDR     a1, Timers_Log

	MOV	a2, #0
	STR	a2, [a1, #TMR_IRQEN] ; All IRQs disabled
;

	LDR	a2,=240000           ; initial value to give centiseconds
	STR	a2, [a1, #TMR_INIT0]
	STR	a2, [a1, #TMR_INIT1]

	MOV	a2, #7  ; continuous, /1 prescale, 24Meg source, auto ,start
	STR	a2, [a1, #TMR_CTRL0]
	STR	a2, [a1, #TMR_CTRL1]
	MOV	a2, #3
	STR	a2, [a1, #TMR_IRQEN] ; enable tmr0 + tmr1 irq
	MOV	pc,lr

HAL_Timers
        MOV     a1, #TimerCount
        MOV     pc, lr

; return IRQ number for timer
HAL_TimerDevice
        CMP     a1, #TimerCount
        MVNHS   a1, #0          ; Error!
        ADRLO   a2, TimerIRQTable
        LDRLO   a1, [a2, a1, lsl #2]
        MOV     pc, lr
TimerIRQTable
        DCD     IRQ_Timer0
        DCD     IRQ_Timer1

HAL_CounterRate
HAL_TimerGranularity
        LDR	a1,=24000000         ; using 24MHz i/p clock
        MOV     pc, lr

HAL_TimerMaxPeriod
        MVN     a1, #0 ; 32-bit timers
        MOV     pc, lr

HAL_TimerSetPeriod
        CMP     a1, #TimerCount
        ; Get pointer to registers
        MOVHS   a1, #0
        LDRLO   pc, [pc, a1, LSL #2]
        MOV     pc, lr
        DCD     Timer_SetPeriod
        DCD     Timer_SetPeriod
; standard timers
Timer_SetPeriod
	MOV	a4, lr
	DebugReg a2, "SET PERIOD: "
	MOV	lr, a4
        ; Get pointer to registers

	LDR     a3, Timers_Log
	CMP     a1, #0
	ADDNE   a3, a3, #TMR_CTRL1-TMR_CTRL0
	MOV	a4, #0
	STR	a4, [a3, #TMR_CTRL0] ;

	CMP     a2, #0
        MOVEQ   pc, lr

        CMP     a2, #1 ;
        MOVEQ   a2, #2
	STR	a2, [a3, #TMR_INIT0] ;

        MOV	a4, #7
	STR	a4, [a3,#TMR_CTRL0]
	MOV	pc, lr

HAL_CounterPeriod
	;Entry	"a1"
        MOV     a1, #0
        ; Fall through

HAL_TimerPeriod
        CMP     a1, #TimerCount
        MOVHS   a1, #0
        LDRLO   pc, [pc, a1, LSL #2]
        MOV     pc, lr
        DCD     Timer_Period
        DCD     Timer_Period
; standard timers
Timer_Period
	;DebugTX	"TimerPeriod"
	LDR     a2, Timers_Log
	CMP     a1, #0
	ADDNE   a2, a2, #TMR_CTRL1-TMR_CTRL0
	LDR	a1, [a2, #TMR_INIT0]
	MOV	pc,lr

HAL_CounterRead
	;Entry	"a1"
        MOV     a1, #0
        ; Fall through

HAL_TimerReadCountdown
        CMP     a1, #TimerCount
        MOVHS   a1, #0
        LDRLO   pc, [pc, a1, LSL #2]
        MOV     pc, lr
        DCD     Timer_ReadCountdown
        DCD     Timer_ReadCountdown
; standard timers
Timer_ReadCountdown
	LDR     a2, Timers_Log ; Get timer 0
	CMP     a1, #0
	ADDNE   a2, a2, #TMR_CTRL1-TMR_CTRL0
        LDR     a1, [a2, #TMR_CUR0] ;
	MOV	pc,lr

; If they want n ticks, wait until we've seen n+1 transitions of the clock.
; This function is limited to delays of around 223.6 seconds. Should be plenty!
HAL_CounterDelay
        MOV	a2,#240
	MUL     a1, a2, a1 ;
        MOV     a4, lr
        BL      __rt_udiv10 ;
        MOV     lr, a4
        LDR     a4, Timers_Log ;
        LDR     a2, [a4, #TMR_CUR0]
10
        LDR     a3, [a4, #TMR_CUR0]

        SUBS    ip, a2, a3              ; ip = difference in ticks
        ADDLO   ip, a2, #1              ; if wrapped, must have had at least old latch+1 ticks
        SUBS    a1, a1, ip
        MOVLO   pc, lr
        LDR     a2, [a4, #TMR_CUR0]
        SUBS    ip, a3, a2              ; ip = difference in ticks
        ADDLO   ip, a3, #1              ; if wrapped, must have had at least old latch+1 ticks
        SUBS    a1, a1, ip
        BHS     %BT10
        MOV     pc, lr

HAL_TimerIRQClear
        CMP     a1, #TimerCount
        MOVHS   a1, #0
        LDR     a2, Timers_Log
        ADD     a1, a1, #1 ; bit 0 or 1 set as appropriate
        STR     a1, [a2, #TMR_IRQSTAT]
        MOV     pc, lr

; HAL_Watchdog exposes access to the watchdog timers
; We will expose the main timer watchdog to the outside world

; On entry, a1 = reason code
; a1 = 0 report capabilities etc
; a1 = 1 Claim/initialise things
; a1 = 2 write watchdog tickle address
; a1 = 3 acknowledge timeout warning irq.

HAL_Watchdog
        cmp     a1, #1
        blt     WDInfo
        bgt     WDTickle
        beq     WDSetup
        cmp     a1, #4
        blt     WDIRQAck
        mov     pc, lr

; WDTickle writes the contents of a2 to the tickle address.
; it is up to the client to provide the correct write data
; which is indicated in the WDInfo response
WDTickle
        ldr     a1, Timers_Log
        str     a2, [a1, #WD0_CTRL]     ; write the tickle register
        MOV     pc, lr
; WDInfo returns a1-> a structure as follows
; flag field to indicate capabilities
; max timeout in 1/2 sec units
; max irq warning time prior to watchdog reset , or 0 if no warning irq
; warning irq number
; tickle value count(TVC)
; list of tickle values to use .. TVC words long
;
WDInfo
        adr     a1, WDInfoList
        mov     pc, lr
; flag field values
WDFlag_HardReset        *       (1<<0)
WDFlag_HasTimeout       *       (1<<1)
WDFlag_HasWarnIRQ       *       (1<<2)
WDFlag_WontTurnOff      *       (1<<3)

WDFlags * (WDFlag_HardReset + WDFlag_HasTimeout + WDFlag_HasWarnIRQ )

WDInfoList
        DCD     WDFlags         ; flag field
        DCD     500             ; unit size in ms for timeouts below
        DCD     16              ; max timeout in steps above
        DCD     0               ; max warning in steps above
        DCD     IRQ_Watchdog    ; watchdog
        DCD     1               ; 1 tickle values to reset timer
        DCD     (&a57<<1)+1     ; (successive) tickle values required

; the watchdog can either intrrrupt, or reset the whole system.
;
; WDSetup is called either with a2 = 0 to turn off the watchdog,
; or with a2 = timeout value to turn the watchdog on
; if a2 <> 0 then if a3 <> 0 also set up the warning irq
; instead of reset
; for any change a4 = &deaddead; else changes ignored
; This assumes the client has already claimed the IRQ
; Pine A64 watchdog doesn't have a warn IRQ
WDSetup
        ldr     a1, =&deaddead
        teq     a4, a1
        movne   pc, lr
        ands    a2, a2, #&ff
        ldr     a1, Timers_Log
        bne     WDTurnOn
        mov     a2, #0                  ; all off
        str     a2, [a1, #WD0_CTRL]     ; write the tickle register
        str     a2, [a1, #WD0_IRQEN]    ; Ensure IRQ disabled
        mov     pc, lr

WDTurnOn
        and     a2, a2, #&ff
        mov     a2, a2, lsl #4
        orr     a2, a2, #&1             ; enable
        str     a2, [a1, #WD0_MODE]     ; write the timer register
        mov     a2, #(&a57<<1)+1
        str     a2, [a1, #WD0_CTRL]     ; write the tickle register
        ands    a3, a3, #&ff
        moveq   a3, #1                  ; reset whole system
        movne   a3, #1<<1               ; interrupt only
        str     a3, [a1, #WD0_CFG]
        moveq   pc, lr                  ; no IRQ needed
; fall through
; Acknowledge the timeout irq bit
WDIRQAck
        ldr     a1, Timers_Log
        mov     a3, #1                  ; IRQ enable and clear IRQ status
        str     a3, [a1, #WD0_IRQSTAT]  ; clear the IRQ
        str     a3, [a1, #WD0_IRQEN]    ; Ensure enabled
        mov     a2, #(&a57<<1)+1
        str     a2, [a1, #WD0_CTRL]     ; write the tickle register
        mov     pc, lr



        END
